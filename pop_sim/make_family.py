import pandas as pd
import numpy as np
from collections import defaultdict
import pickle

def main():
    fam = pd.read_csv('synthetic_persons.csv',dtype=str)
    fam = fam.sort_values(by=['SEZ2011'])

    fam = fam.set_index(fam.household_id)
    fam.index = fam.index.astype(int)
    people = {'inf':0,'kid':0,'hwife':0,'full_m':0,'full_f':0,'no_work_m':0,'no_work_f':0,'old_m':0,'old_f':0,'stud':0,'partial_m':0,'partial_f':0,'work_m':0,'work_f':0}


    houses = defaultdict(str)
    check = 0
    for i,p in fam.iterrows():
        if i != check:
            houses[str(i)] = people.copy()
            print('new')
        check = i
        print(i)
        if p.KIND == '1':
            houses[str(i)]['inf'] += 1
        if p.KIND == '2':
            houses[str(i)]['stud'] += 1
        if p.KIND == '3':
            houses[str(i)]['hwife'] += 1
        if p.KIND == '4':
            houses[str(i)]['work_m'] += 1
        if p.KIND == '5':
            houses[str(i)]['no_work_m'] += 1
        if p.KIND == '6':
            houses[str(i)]['old_m'] += 1

        if p.KIND == '7':
            houses[str(i)]['work_f'] += 1
        if p.KIND == '8':
            houses[str(i)]['no_work_f'] += 1
        if p.KIND == '9':
            houses[str(i)]['old_f'] += 1

        
    
    fname = 'persons.pkl'
    with open(fname, "wb") as f:
        pickle.dump(houses,f)
            
               
         
if __name__ == "__main__":
    import sys
    main()

'''
        if p.COND_PROF=='1':
            if p.SEX=='1':
                houses[str(i)]['work_m']+=1
            else:
                houses[str(i)]['work_f']+=1
        elif p.COND_PROF=='2' or p.COND_PROF=='3':
            if p.SEX=='1':
                houses[str(i)]['no_work_m']+=1
            else:
                houses[str(i)]['no_work_f']+=1            
        elif p.COND_PROF=='4':
            if p.SEX=='1':
                houses[str(i)]['old_m']+=1
            else:
                houses[str(i)]['old_f']+=1
        elif p.COND_PROF=='5':
            if p.AGE=='1':            
                houses[str(i)]['kid']+=1
            else:
                houses[str(i)]['stud']+=1
        elif p.COND_PROF=='6':
           houses[str(i)]['hwife']+=1
        elif p.COND_PROF==' ':
           if p.AGE=='1':            
                houses[str(i)]['infant']+=1


                '''