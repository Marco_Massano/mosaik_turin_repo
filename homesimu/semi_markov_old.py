from __future__ import division
from collections import defaultdict
from functools import partial
from numba import njit
import numpy as np
import datetime
import pickle as pickle

import pathlib
path=pathlib.Path(__file__).parent.absolute()

@njit
def rep(row):
    true_started = False
    c = 0
    for i, j in enumerate(row):
        if true_started and not j:
            return c
        else:
            c += j
            true_started = true_started or j
    return c


def addTime(time, hour):
    fulldate = datetime.datetime(100, 1, 1, time.hour, time.minute, time.second)
    fulldate = fulldate + datetime.timedelta(hours=hour)
    return fulldate.time()

class SemiMarkov(object):
    """docstring for SemiMarkov"""
    def __init__(self):
        self.tran={}
        self.s1=None
        self.s2=None
        self.dur=None

    def add(self, s1, s2, dur):
        if s1 not in self.tran.keys():
            self.tran[s1]={}
            self.tran[s1][s2]={'counter':0,'duration':defaultdict(int)}
            self.tran[s1][s2]['counter'] += 1
            self.tran[s1][s2]['duration'][dur] += 1
        elif s2 not in self.tran[s1].keys():
            self.tran[s1][s2]={'counter':0,'duration':defaultdict(int)}
            self.tran[s1][s2]['counter'] += 1
            self.tran[s1][s2]['duration'][dur] += 1
        else:
            self.tran[s1][s2]['counter'] += 1
            self.tran[s1][s2]['duration'][dur] += 1

    def add_batch(self, paths):
        for i,path in enumerate(paths):
            for s1, s2, d in path:
                self.add(s1, s2, d)
        return self.tran

    def get_tot(self,s1):
        tot=sum((self.tran[s1][s]['counter'] for s in self.tran[s1].keys()),0.0)
        #tot=(sum(self.tran[s1][s]['counter'],0.0) for s in self.tran[s1].keys())
        return tot

    def get_tot1(self,s1):
        tot=0
        for s in self.tran[s1].keys():
            if s!=s1:
                tot=tot+self.tran[s1][s]['counter']
        #tot=(sum(self.tran[s1][s]['counter'],0.0) for s in self.tran[s1].keys())
        return tot

    def get_state_cdf(self,s1):
        cdf={}
        for s in self.tran[s1].keys():
            if s!=s1:
                cdf[s]=self.tran[s1][s]['counter']/self.get_tot1(s1)
        return cdf
        '''
        cdf={}
        for s in self.tran[s1].keys():
            cdf[s]=self.tran[s1][s]['counter']/self.get_tot(s1)
        return cdf
        '''
    def get_dur_cdf(self,s1,s2):
        cdf=self.tran[s1][s2]['duration']
        tot=sum(iter(cdf.values()),0.0)
        cdf = {k: v / tot for k, v in iter(cdf.items())}
        return cdf

    def set_first_state(self):
        cdf={}
        for s in self.tran.keys():
            cdf[s]=self.get_tot(s)
        tot=sum(iter(cdf.values()),0.0)
        cdf = {k: v / tot for k, v in iter(cdf.items())}
        self.s1=np.random.choice(list(cdf.keys()),p=list(cdf.values()))
        dur_cdf=self.get_dur_cdf(self.s1,self.s1)
        try:
            self.dur=np.random.choice(list(dur_cdf.keys()),p=list(dur_cdf.values()))-1
        except:
            self.dur=1
        

    def next_state(self):
        try:
            state_cdf=self.get_state_cdf(self.s1)
            self.s2=np.random.choice(state_cdf.keys(),p=list(state_cdf.values()))
            dur_cdf=self.get_dur_cdf(self.s1,self.s2)
            self.dur=np.random.choice(list(dur_cdf.keys()),p=list(dur_cdf.values()))-1
            self.s1=self.s2
            if self.s1==None:
                self.set_first_state() 
        except:
            self.set_first_state()

           

    def reset(self):
        self.__init__()

class NHSMM(SemiMarkov):
    """docstring for NHSMM"""
    def __init__(self):
        super(NHSMM, self).__init__()        
        self.model={}
        self.state=None
        self.dur_n=None
    
    def create(self,tav):
        index=tav.columns.values
        tav=tav.values

        for t in range(tav.shape[1]-1):
            b=[]
            for j in range(tav.shape[0]):
                s1=tav[j,t]
                s2=tav[j,t+1]
                d=self.repetition(np.append(tav[j,t+1:],tav[j,:t+1])==tav[j,t+1])
                b.append([(s1,s2,d)])
            self.model[datetime.datetime.strptime(index[t],'%H:%M:%S').time()]=self.add_batch(b)
        
        t+=1
        b=[]
        for j in range(tav.shape[0]):
            s1=tav[j,t]
            s2=tav[j,0]
            d=self.repetition(tav[j,:]==tav[j,0])
            b.append([(s1,s2,d)])
        self.model[datetime.datetime.strptime(index[t],'%H:%M:%S').time()]=self.add_batch(b)

    def start(self,time):
        self.tran=self.model[time]
        self.set_first_state()
        self.state=self.s1
        self.dur_n=self.dur


    def next(self,time):
        self.s1=self.state
        self.next_state()
        self.state=self.s1
        self.dur_n=self.dur

    def repetition(self,a):
        a = np.concatenate([[False], a, [False]])
        return np.argmin(a[np.argmax(a):])

    def save(self,name):
        fname=name+'.pkl'

        f = open(path.joinpath('data/input/tran_matr/'+fname), "wb")
        pickle.dump(self.model,f)#,protocol=0)
        f.close()

    def load(self,name):
        fname=name+'.pkl' 
        f = open(fname, "rb")
        self.model=pickle.load(f)


    def reset(self):
        self.__init__()
