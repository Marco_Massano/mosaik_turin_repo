from . import Configuration
import numpy as np
from . tansition_new import Transition as tran
from . import semi_markov as sm
from collections import defaultdict
import pandas as pd
import os
from memory_profiler import profile

path= os.path.dirname(__file__) + "/data"

dact=pd.read_csv(path + '/catpri.csv', dtype=str, index_col=0,header=None,squeeze=1).to_dict()
dact1=pd.read_csv(path + '/catcon.csv', dtype=str, index_col=0,header=None,squeeze=1).to_dict()
tran1=tran()



class Person(object):
    def __init__(self):
        self.week=sm.NHSMM()
        self.sat=sm.NHSMM()
        self.sun=sm.NHSMM()
        #self.chain={}
        self.person_id=0
        self.wait=0     
        self.chain=defaultdict(int)
        for act in np.unique(list(dact.values())):
            self.chain[act]=0
        self.chain['Presence']=1
        self.chain['Adult']=0
        self.chain['Active']=0
        self.chain['Shower']=0

        self.had_shower=False
        self.is_shower=False
        self.count=0

    def add_state_step(self,time,act):

        act=act.split('_')
        act1=dact1[act[1]]
        act=dact[act[0]]
        self.wait=int(self.wait)
        if act=='Absent':
            self.chain['Presence']=0
            self.chain[act]=1
        else:
            self.chain['Presence']=1
            self.chain[act]=1
            if act1!='00':
                self.chain[act1]=1
        if act!='Absent' and act!='Sleep':
            self.chain['Active']=1
            if self.adult==True:
                self.chain['Adult']=1
        if act=='Bathroom' and self.had_shower==False and self.wait>=3:
            self.chain['Shower']=1
            self.had_shower==True
            self.is_shower=True
        

    def simulate_step(self,time):

        #time=days[0]
        if self.count==0:
            self.day=time.day

        if time.day!=self.day:
            self.day=time.day
            self.had_shower=False
            self.is_shower=False

        if time.dayofweek==5 and time.time().hour==0 and time.time().minute==0:
            self.sat.state=self.week.state

        if time.dayofweek==6 and time.time().hour==0 and time.time().minute==0:
            self.sun.state=self.sat.state

        if time.dayofweek==0 and time.time().hour==0 and time.time().minute==0:
            self.week.state=self.sun.state

        if self.wait == 0:
            if time.dayofweek<5:
                if self.count == 0:
                    self.week.start(time.time())
                else:
                    self.week.next(time.time())

                self.add_state_step(time,self.week.state)
                self.wait=self.week.dur
                
            elif time.dayofweek==5:
                if len(self.chain) == 0:
                    self.sat.start(time.time())
                else:
                    self.sat.next(time.time())
                self.add_state_step(time,self.sat.state)
                self.wait=self.sat.dur
                
            elif time.dayofweek==6:
                if len(self.chain) == 0:
                    self.sun.start(time.time())
                else:
                    self.sun.next(time.time())
                self.add_state_step(time,self.sun.state)
                self.wait=self.sat.dur
        else:
            if time.dayofweek<5:
                self.wait=self.wait-1

            if time.dayofweek==5:
                self.wait=self.wait-1
                
            if time.dayofweek==6:
                self.wait=self.wait-1

        self.count+=1

class Infant(Person):
    def __init__(self,pid):
        super(Infant, self).__init__()
        self.week=tran1.infantWeek
        self.sat=tran1.infantSat
        self.sun=tran1.infantSun
        self.person_id=pid
        self.adult=False
        self.type='Infant'

class Kid(Person):
    def __init__(self,pid):
        super(Kid, self).__init__()
        self.week=tran1.kidsWeek
        self.sat=tran1.kidsSat
        self.sun=tran1.kidsSun
        self.person_id=pid
        self.adult=False
        self.type='Kid'

class Stud(Person):
    def __init__(self,pid):
        super(Stud, self).__init__()
        self.week=tran1.studWeek
        self.sat=tran1.studSat
        self.sun=tran1.studSun
        self.person_id=pid
        self.adult=False
        self.type='Stud'

class Hwife(Person):
    def __init__(self,pid):
        super(Hwife, self).__init__()
        self.week=tran1.hwifeWeek
        self.sat=tran1.hwifeSat
        self.sun=tran1.hwifeSun
        self.person_id=pid
        self.adult=True
        self.type='Hwife'
            
class WorkM(Person):
    def __init__(self,pid):
        super(WorkM, self).__init__()
        self.week=tran1.workmWeek
        self.sat=tran1.workmSat
        self.sun=tran1.workmSun
        self.person_id=pid
        self.adult=True
        self.type='WorkM'

class WorkF(Person):
    def __init__(self,pid):
        super(WorkF, self).__init__()
        self.week=tran1.workfWeek
        self.sat=tran1.workfSat
        self.sun=tran1.workfSun
        self.person_id=pid
        self.adult=True
        self.type='WorkF'

class NoWorkM(Person):

    def __init__(self,pid):
        super(NoWorkM, self).__init__()
        self.week=tran1.noworkmWeek
        self.sat=tran1.noworkmSat
        self.sun=tran1.noworkmSun   
        self.person_id=pid
        self.adult=True
        self.type='NoWorkM'

class NoWorkF(Person):
    def __init__(self,pid):
        super(NoWorkF, self).__init__()
        self.week=tran1.noworkfWeek
        self.sat=tran1.noworkfSat
        self.sun=tran1.noworkfSun
        self.person_id=pid
        self.adult=True
        self.type='NoWorkF'

class OldM(Person):
    def __init__(self,pid):
        super(OldM, self).__init__()
        self.week=tran1.oldmWeek
        self.sat=tran1.oldmSat
        self.sun=tran1.oldmSun
        self.person_id=pid
        self.adult=True
        self.type='OldF'

class OldF(Person):
    def __init__(self,pid):
        super(OldF, self).__init__()
        self.week=tran1.oldfWeek
        self.sat=tran1.oldfSat
        self.sun=tran1.oldfSun
        self.person_id=pid
        self.adult=True
        self.type='OldM'


class FullM(Person):
    def __init__(self,pid):
        super(FullM, self).__init__()
        self.week=tran1.fullmWeek
        self.sat=tran1.fullmSat
        self.sun=tran1.fullmSun      
        self.person_id=pid
        self.adult=True
        self.type='FullM'

class FullF(Person):
    def __init__(self,pid):
        super(FullF, self).__init__()
        self.week=tran1.fullfWeek
        self.sat=tran1.fullfSat
        self.sun=tran1.fullfSun
        self.person_id=pid
        self.adult=True
        self.type='FullF'

class PartialM(Person):
    def __init__(self,pid):
        super(PartialM, self).__init__()
        self.week=tran1.partialmWeek
        self.sat=tran1.partialmSat
        self.sun=tran1.partialmSun  
        self.person_id=pid
        self.adult=True
        self.type='PartialM'

class PartialF(Person):
    def __init__(self,pid):
        super(PartialF, self).__init__()
        self.week=tran1.partialfWeek
        self.sat=tran1.partialfSat
        self.sun=tran1.partialfSun   
        self.person_id=pid
        self.adult=True
        self.type='PartialF'



