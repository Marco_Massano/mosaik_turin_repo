from semi_markov import NHSMM#as sm
import os


path = os.path.dirname(__file__) + '/data/input/tran_matr'

infantWeek=NHSMM()
infantSat=NHSMM()
infantSun=NHSMM()
infantWeek.load(os.path.join(path,'infant_week'))
infantSat.load(os.path.join(path,'infant_sat'))
infantSun.load(os.path.join(path,'infant_sun'))

def get_infant():
    return infantWeek,infantSat,infantSun

kidsWeek=NHSMM()
kidsSat=NHSMM()
kidsSun=NHSMM()
kidsWeek.load(os.path.join(path,'kids_week'))
kidsSat.load(os.path.join(path,'kids_sat'))
kidsSun.load(os.path.join(path,'kids_sun'))

def get_kids():
    return kidsWeek,kidsSat,kidsSun

studWeek=NHSMM()
studSat=NHSMM()
studSun=NHSMM()
studWeek.load(os.path.join(path,'stud_week'))
studSat.load(os.path.join(path,'stud_sat'))
studSun.load(os.path.join(path,'stud_sun'))

def get_stud():
    return studWeek,studSat,studSun


hwifeWeek=NHSMM()
hwifeSat=NHSMM()
hwifeSun=NHSMM()
hwifeWeek.load(os.path.join(path,'hwife_week'))
hwifeSat.load(os.path.join(path,'hwife_sat'))
hwifeSun.load(os.path.join(path,'hwife_sun'))

def get_hwife():
    return hwifeWeek,hwifeSat,hwifeSun

workmWeek=NHSMM()
workmSat=NHSMM()
workmSun=NHSMM()
workmWeek.load(os.path.join(path,'work_m_week'))
workmSat.load(os.path.join(path,'work_m_sat'))
workmSun.load(os.path.join(path,'work_m_sun'))

def get_workm():
    return workmWeek,workmSat,workmSun

workfWeek=NHSMM()
workfSat=NHSMM()
workfSun=NHSMM()
workfWeek.load(os.path.join(path,'work_f_week'))
workfSat.load(os.path.join(path,'work_f_sat'))
workfSun.load(os.path.join(path,'work_f_sun'))

def get_workf():
    return workfWeek,workfSat,workfSun

noworkmWeek=NHSMM()
noworkmSat=NHSMM()
noworkmSun=NHSMM()
noworkmWeek.load(os.path.join(path,'no_work_m_week'))
noworkmSat.load(os.path.join(path,'no_work_m_sat'))
noworkmSun.load(os.path.join(path,'no_work_m_sun'))

def get_noworkm():
    return noworkmWeek,noworkmSat,noworkmSun

noworkfWeek=NHSMM()
noworkfSat=NHSMM()
noworkfSun=NHSMM()
noworkfWeek.load(os.path.join(path,'no_work_f_week'))
noworkfSat.load(os.path.join(path,'no_work_f_sat'))
noworkfSun.load(os.path.join(path,'no_work_f_sun'))

def get_noworkf():
    return noworkfWeek,noworkfSat,noworkfSun

oldmWeek=NHSMM()
oldmSat=NHSMM()
oldmSun=NHSMM()
oldmWeek.load(os.path.join(path,'old_m_week'))
oldmSat.load(os.path.join(path,'old_m_sat'))
oldmSun.load(os.path.join(path,'old_m_sun'))

def get_oldm():
    return oldmWeek,oldmSat,oldmSun

oldfWeek=NHSMM()
oldfSat=NHSMM()
oldfSun=NHSMM()
oldfWeek.load(os.path.join(path,'old_f_week'))
oldfSat.load(os.path.join(path,'old_f_sat'))
oldfSun.load(os.path.join(path,'old_f_sun'))

def get_oldf():
    return oldfWeek,oldfSat,oldfSun


fullfWeek=NHSMM()
fullfSat=NHSMM()
fullfSun=NHSMM()
fullfWeek.load(os.path.join(path,'fullwork_f_week'))
fullfSat.load(os.path.join(path,'fullwork_f_sat'))
fullfSun.load(os.path.join(path,'fullwork_f_sun'))

def get_fullf():
    return fullfWeek,fullfSat,fullfSun
    

fullmWeek=NHSMM()
fullmSat=NHSMM()
fullmSun=NHSMM()
fullmWeek.load(os.path.join(path,'fullwork_m_week'))
fullmSat.load(os.path.join(path,'fullwork_m_sat'))
fullmSun.load(os.path.join(path,'fullwork_m_sun'))

def get_fullm():
    return fullmWeek,fullmSat,fullmSun    

partialfWeek=NHSMM()
partialfSat=NHSMM()
partialfSun=NHSMM()
partialfWeek.load(os.path.join(path,'partialwork_f_week'))
partialfSat.load(os.path.join(path,'partialwork_f_sat'))
partialfSun.load(os.path.join(path,'partialwork_f_sun'))

def get_Partialf():
    return partialfWeek,partialfSat,partialfSun
    

partialmWeek=NHSMM()
partialmSat=NHSMM()
partialmSun=NHSMM()
partialmWeek.load(os.path.join(path,'partialwork_m_week'))
partialmSat.load(os.path.join(path,'partialwork_m_sat'))
partialmSun.load(os.path.join(path,'partialwork_m_sun'))

def get_Partialm():
    return partialmWeek,partialmSat,partialmSun    
    
    

