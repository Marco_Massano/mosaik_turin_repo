from . Person_new import *
from . device_new  import *
from collections import defaultdict
from functools import partial
from . import semi_markov as sm
import os
import time


path = os.path.dirname(__file__) + "/data"
dact = pd.read_csv(path + '/catpri.csv', dtype = str, index_col = 0,header = None,squeeze = 1).to_dict()
dact1 = pd.read_csv(path + '/catcon.csv', dtype = str, index_col = 0,header = None,squeeze = 1).to_dict()


class Home:

    def __init__(self,people={'hwife': 1,'inf': 1,'kid': 2,'no_work_f': 0,'no_work_m': 0,'old_f': 0,'old_m': 0,'stud': 0,'work_f': 1,
            'work_m':1,'full_m':1,'full_f':0,'partial_m':0,'partial_f':0}):
        dishint  = 0
        washint = 0
        dryint = 0
        self.people=people
        self.id_house = 0
        self.personsList={}
        self.appliancesList={}
        self.RoomsNum = 0
        self.floorsize = 0
        self.inf = people['inf']
        self.kid = people['kid']
        self.hwife = people['hwife']
        self.work_m = people['work_m']
        self.work_f = people['work_f']
        self.no_work_m = people['no_work_m']
        self.no_work_f = people['no_work_f']
        self.old_m = people['old_m']
        self.old_f = people['old_f']
        self.full_f = people['full_f']
        self.full_m = people['full_m']
        self.partial_f = people['partial_f']
        self.partial_m = people['partial_m']
        self.stud = people['stud']
        self.weeks = 0
        self.wash_left={'week':0,'w_end':0}
        self.dish_left={'week':0,'w_end':0}
        self.hoven_left=0
        self.day = 0
        self.has_wash = False
        self.has_dish = False
        self.has_dryer = False
        self.hasBoiler = False
        self.hasHoven = False
        self.hasMicro = False
        self.hovenday = False
        self.washday = False
        self.dishday = False
        self.dryday = False
        self.days = Configuration.getDays()
        self.res= Configuration.appliances_res
        self.Consumption=defaultdict(int)
        self.step=0
        self.step_behavior=0
        self.caled=0
        self.time_wash=0
        self.time_dish=0
        self.generate_house()
        self.houseBehaviour=defaultdict(int)
        self.reset_houseBehaviour()
        self.reset_consumption()
        if self.hasBoiler==True:
            self.doneWash=False
            self.ACS=0
        self.had_shower=dict.fromkeys(self.personsList.keys(),False)
        self.is_shower=dict.fromkeys(self.personsList.keys(),False)
        self.appliaces=list(self.appliancesList.keys())
    
    def reset_consumption(self):
        for name in self.appliancesList:
            self.Consumption[name]=0
            self.Consumption['Aggregate']=0
            self.Consumption['Lights']=0
            self.Consumption['TV']=0


    def reset_houseBehaviour(self):
        for act in np.unique(list(dact.values())):
            self.houseBehaviour[act]=0
        self.houseBehaviour['Presence']=0
        self.houseBehaviour['Adult']=0        
        self.houseBehaviour['Washingmachine']=0
        self.houseBehaviour['Dryer']=0
        self.houseBehaviour['Dishwasher']=0
        self.houseBehaviour['Hoven']=0
        self.houseBehaviour['Shower']=0

    def simulate_behaviour_step(self,time):
        if self.step_behavior==0:
            self.day=time.day
        
        if self.hasBoiler==True:
            self.ACS=0

        if self.hasHoven is not False:
            self.hoven_left=self.appliancesList['Hoven'].number        
            if self.weeks!=(self.days[-1]-time).days/7:
                self.weeks=(self.days[-1]-time).days/7
                self.hoven_left=self.appliancesList['Hoven'].number
            if self.step_behavior==0 or self.day!=time.day:
                self.hovenday = False
                self.hovenday = self.isHovenDay(time)
        if self.has_wash is not False:
            washint=self.appliancesList['Washingmachine'].lenTrace
            self.wash_left['week']=self.appliancesList['Washingmachine'].number
            self.wash_left['w_end']=self.appliancesList['Washingmachine'].number_we   
            if self.weeks!=(self.days[-1]-time).days/7:
                self.weeks=(self.days[-1]-time).days/7
                self.wash_left['week']=self.appliancesList['Washingmachine'].number
                self.wash_left['w_end']=self.appliancesList['Washingmachine'].number_we
            if self.step_behavior==0 or self.day!=time.day:
                self.washday = False
                self.washday = self.isWashingDay(time)
                if self.washday==True:
                    self.time_wash=self.SetWashtime(time)
                    washint=self.appliancesList['Washingmachine'].lenTrace
        else:
            washint=0

        if self.has_dryer is False:
            dryint=0
        else:
            dryint=self.appliancesList['Dryer'].lenTrace
        if (self.step_behavior==0 or self.day!=time.day) and self.has_dryer is not False and self.dryday is not False:
            dryint=self.appliancesList['Dryer'].lenTrace

        if self.has_dish is not False:
            dishint=self.appliancesList['Dishwasher'].lenTrace
            self.dish_left['week']=self.appliancesList['Dishwasher'].number
            self.dish_left['w_end']=self.appliancesList['Dishwasher'].number_we
            if self.weeks!=(self.days[-1]-time).days/7:
                self.weeks=(self.days[-1]-time).days/7
                self.dish_left['week']=self.appliancesList['Dishwasher'].number
                self.dish_left['w_end']=self.appliancesList['Dishwasher'].number_we
            if self.step_behavior==0 or self.day!=time.day:
                self.dishday = False
                self.dishday = self.isDishingDay(time)
                if self.dishday==True:
                    self.time_dish=self.SetDishtime(time)
                    dishint=self.appliancesList['Dishwasher'].lenTrace
        else:
            dishint=0
        
        self.reset_houseBehaviour()
        hovenint = 0

        #simualte personal behaviour
        for name,person in iter(self.personsList.items()):
            person.simulate_step(time)
            hovenint=0
            for act in person.chain.keys():
                self.houseBehaviour[act]+=person.chain[act]
            self.had_shower[name]=person.had_shower
            self.is_shower[name]=person.is_shower

            if self.hovenday==True and self.houseBehaviour['Cooking']>0:
                hovenint=person.wait
            elif self.hovenday==False and self.houseBehaviour['Cooking']==0:
                hovenint=0

            if self.hasBoiler==True:
                if self.step_behavior==0 or self.day!=time.day:
                    self.had_shower[name]=False
                if not any(self.is_shower.values()):
                    if person.chain['Bathroom']>=1 and person.wait >= 3 and self.had_shower[name]==False:
                        #########
                        self.ACS=1
                        self.had_shower[name]=True
                        self.is_shower[name]=True    
                if person.chain['Shower']==0 and self.is_shower[name]==True:
                    self.is_shower[name]=False
        #simulate house behaviour
        
        #metter in ciclo for persone
        if self.hovenday==True and hovenint!=0: # TODO problema referenced
            if dishint==0 and washint==0 and dryint==0 and self.houseBehaviour['Ironing']==0:
                self.houseBehaviour['Hoven']=1
                self.hovenday = False
            elif self.hovenday==False and hovenint>0:
                self.houseBehaviour['Hoven']=1

        #set washingmachine activation
            
            if self.washday==True and (self.time_wash<=time.time() and self.houseBehaviour['Adult']>0):
                if hovenint==0 and self.houseBehaviour['Ironing']==0 and dishint==0 and dryint==0:
                    self.houseBehaviour['Washingmachine']=1
                    self.dryday=True
                    self.washday = False
                    washint-=1
            elif self.washday==False and washint>0:
                washint-=1
                self.houseBehaviour['Washingmachine']=1

        #set dishwasher activation
            if self.dishday==True and (self.time_dish<=time.time() and self.houseBehaviour['Adult']>0):
                if hovenint==0 and (self.houseBehaviour['Ironing']==0 and washint==0 and dryint==0):
                    self.houseBehaviour['Dishwasher']=1
                    self.dishday = False
            elif self.dishday == False and dishint > 0:
                dishint-=1
                self.houseBehaviour['Dishwasher']=1

            if self.dryday == True and self.houseBehaviour['Washingmachine']==1:
                self.houseBehaviour['Dryer']=1
                self.dryday=False
                dryint-=1
            elif self.dryday == False and dryint>0:
                self.houseBehaviour['Dryer']=1
                dryint-=1

        #set ACS USAGE activation

        if not self.has_dish and self.hasBoiler:
            if self.houseBehaviour['Dishwashing']>=1 and self.doneWash==False and  any(self.is_shower.items()):
                self.doneWash=True
                self.ACS=2
            if self.houseBehaviour['Dishwashing']==0:
                self.doneWash=False


        #self.houseBehaviour=self.houseBehaviour.fillna(value=0)


        if self.has_wash:
            self.houseBehaviour['Washing']=0
        if self.has_dish:
            self.houseBehaviour['Dishwashing']=0

        self.step_behavior+=1

        if time.day!=self.day:
            self.day=time.day


    def simulate_consumption_step(self,time):

        for name,app in iter(self.appliancesList.items()):
            if name in self.houseBehaviour.keys():
                app.simulate(time,self.houseBehaviour[name])

            elif name.startswith('TV'):
                if int(name.split('TV')[1])<self.houseBehaviour['TV']:
                    app.simulate(time,self.houseBehaviour['TV'])
                else:
                    app.add_0()
                    app.status=0

            elif name.startswith('PC'):
                if int(name.split('PC')[1])<self.houseBehaviour['PC']:
                    app.simulate(time,self.houseBehaviour['PC'])
                else:
                    app.add_0()
                    app.status=0
            
            elif name == 'Leaving':
                app.simulate(time,self.houseBehaviour['TV']+self.houseBehaviour['Eating'])
            
            elif name in self.personsList.keys():
                app.simulate(time,self.personsList[name].chain['Active'])

            elif name in ['Fridge','Microwave']:
                app.simulate(time,self.houseBehaviour['Cooking']+self.houseBehaviour['Eating'])
            
            elif name == 'Dryer':
                app.simulate(time,self.houseBehaviour['Washingmachine'])
            
            elif name == 'Vacuumcleaner':
                app.simulate(time,self.houseBehaviour['Cleaning'])
            
            elif name == 'Iron':
                app.simulate(time,self.houseBehaviour['Ironing'])
            
            elif name == 'Boiler':
                app.simulate(self.ACS)          
            
            elif name == 'Elecooker':
                if self.houseBehaviour['Hoven']==0:
                    app.simulate(time,self.houseBehaviour['Cooking'])
                else:
                    app.simulate(time,0)
        '''
        for name,app in self.appliancesList.iteritems():
            if app.type=='Light':
                self.Consumption['Lights']+=app.Profile
            else:
                if name=='Boiler':
                    self.resize_boiler()
                self.Consumption[name]+=app.Profile
                
            self.Consumption['Aggregate']+=app.Profile
        '''
        self.reset_consumption()
        for name,app in iter(self.appliancesList.items()):
            if app.type=='Light':
                self.Consumption['Lights']+=float(app.Profile)
                self.Consumption['Aggregate']+=float(app.Profile)
            else:
                if name=='Boiler':
                    self.resize_boiler_step()
                    self.Consumption[name]+=float(app.Profile)
                    self.Consumption['Aggregate']+=float(app.Profile)
                else:
                    self.Consumption[name]+=float(app.Profile)
                    self.Consumption['Aggregate']+=float(app.Profile)
            if app.type=='TV':
                self.Consumption['TV']+=float(app.Profile)

        self.step+=1

    
    def resize_boiler_step(self):
        if self.res>1:
            self.appliancesList['Boiler'].Profile=self.appliancesList['Boiler'].LoadTrace[self.step*self.res:].reshape(-1,self.res).mean(axis=1)
            return 

    def simulate_step(self,time):
        self.simulate_behaviour_step(time)
        self.simulate_consumption_step(time)

    def isHovenDay(self,time):
        rand = random.random()
        if rand<self.hoven_left/7.0:
            return True
        else:
            return False
    

    def isWashingDay(self,time):
        rand = random.random()
        day = time.dayofweek
        if  day < 5:
            den = 5
            if rand<self.wash_left['week']/float(den):
                return True
            else:
                return False
        if day >= 5:
            if rand<self.wash_left['w_end']/2:
                self.wash_left['w_end']
                return True
            else:
                return False
    
    def isDishingDay(self,time):
        rand = random.random()
        day = time.dayofweek
        if  day < 5:
            den = 5
            if rand<self.dish_left['week']/float(den):
                return True
            else:
                return False
        if day >= 5:
            if rand<self.dish_left['w_end']/2:
                self.dish_left['w_end']-=1
                return True
            else:
                return False

    def SetWashtime(self,time):
        rand=random.random()
        wash_cdf= Configuration.getwash_cdf(self.personsList, time)
        time_wash=wash_cdf.index[np.argmin(wash_cdf[1]<=rand)]
        return time_wash

    def startwash(self,time):
        if ('WorkF_0' in self.personsList):
            if self.personsList['WorkF_0'].chain.loc[time,'Presence']>=1:
                rand = random.random()
                if rand< Configuration.getwash_cdf(time, self.personsList):
                    return True
                else:
                    return False
            else:
                return False
        else:
            if self.houseBehaviour.loc[time,'Adult']>=1:
                rand = random.random()
                if rand< Configuration.getwash_cdf(time, self.personsList):
                    return True
                else:
                    return False
            else:
                return False
    
    def SetDishtime(self,time):
        rand=random.random()
        dish_cdf= Configuration.getdish_cdf(self.personsList, time)#okkio che se torni indietro togli time
        time_dish=dish_cdf.index[np.argmin(dish_cdf[1]<=rand)]
        return time_dish

    def startdish(self,time):
        if ('WorkF_0' in self.personsList):
            if self.personsList['WorkF_0'].chain.loc[time,'Presence']>=1:
                rand = random.random()
                if rand< Configuration.getdish_cdf(time, self.personsList):
                    return True
                    
                else:
                    return False
            else:
                return False
        else:
            if self.houseBehaviour.loc[time,'Adult']>=1:
                rand = random.random()
                if rand< Configuration.getdish_cdf(time, self.personsList):
                    return True
                else:
                    return False
            else:
                return False

    def generate_persons(self):
        
        person = None
        
        for j in range(0,self.inf):
            person = Infant('Infant_'+str(j))
            self.personsList['Infant_'+str(j)]=person

        for j in range(0,self.kid):
            person = Kid('Kid'+str(j))
            self.personsList['Kid_'+str(j)]=person

        for j in range(0,self.stud):
            person = Stud('Stud_'+str(j))
            self.personsList['Stud_'+str(j)]=person

        for j in range(0,self.hwife):
            person = Hwife('Hwife_'+str(j))
            self.personsList['Hwife_'+str(j)]=person

        for j in range(0,self.work_m):
            person = WorkM('WorkM_'+str(j))
            self.personsList['WorkM_'+str(j)]=person

        for j in range(0,self.work_f):
            person = WorkF('WorkF_'+str(j))
            self.personsList['WorkF_'+str(j)]=person

        for j in range(0,self.no_work_m):
            person = NoWorkM('NoWorkM_'+str(j))
            self.personsList['NoWorkM_'+str(j)]=person

        for j in range(0,self.no_work_f):
            person = NoWorkF('NoWorkF_'+str(j))
            self.personsList['NoWorkF_'+str(j)]=person

        for j in range(0,self.old_m):
            person = OldM('OldM_'+str(j))
            self.personsList['OldM_'+str(j)]=person

        for j in range(0,self.old_f):
            person = OldF('OldF_'+str(j))
            self.personsList['OldF_'+str(j)]=person
        
        for j in range(0,self.full_f):
            person = FullF('FullF_'+str(j))
            self.personsList['FullF_'+str(j)]=person
            
        for j in range(0,self.full_m):
            person = FullM('FullM_'+str(j))
            self.personsList['FullM_'+str(j)]=person
            
        for j in range(0,self.partial_f):
            person = PartialF('PartialF_'+str(j))
            self.personsList['PartialF_'+str(j)]=person
        
        for j in range(0,self.partial_m):
            person = PartialM('PartialM_'+str(j))
            self.personsList['PartialM_'+str(j)]=person
            
        self.RoomsNum = len(self.personsList)+3
        self.FamSize = len(self.personsList)

    def generate_appliances(self):

        #Lights
        for person in self.personsList:
            self.appliancesList[person]=Lights(person)
        self.appliancesList['Cooking']=Lights('Cooking')
        self.appliancesList['Leaving']=Lights('Leaving')
        self.appliancesList['Bathroom']=Lights('Bathroom')

        #Washingmachine
        data = False
        data = Configuration.isWashing(self.FamSize)
        if data!=False:
            self.appliancesList['Washingmachine']=Washingmachine(data)
            self.has_wash = True
            self.wash_left['week']=data['#w_cycle']
            self.wash_left['w_end']=data['#wend_cycle']
        data = False        

        #Dishwasher
        data = Configuration.isDishwash(self.FamSize)
        if data!=False:
            self.appliancesList['Dishwasher']=Dishwasher(data)
            self.has_dish = True
            self.dish_left['week']=data['#w_cycle']
            self.dish_left['w_end']=data['#wend_cycle']

        #TV
        self.TVNumber = Configuration.isTV(self.FamSize)
        for i in range(self.TVNumber):
            self.appliancesList['TV'+str(i)]=Television()

        #PC_Desktop
        self.DesktopNumber = Configuration.isDesktop(self.FamSize)
        if self.DesktopNumber:
            for i in range(self.DesktopNumber):
                self.appliancesList['PC'+str(i)]=Desktop()

        #Laptop
        self.LaptopNumber = Configuration.isLaptop(self.FamSize)
        if self.LaptopNumber:
            for i in range(self.DesktopNumber,self.LaptopNumber+self.DesktopNumber):
                self.appliancesList['PC'+str(i)]=Laptop()

        #Fridge
        data=False
        data = Configuration.isFridge(self.FamSize)
        if data!=False:
            self.appliancesList['Fridge']=Fridge(data)
        
        #Hoven
        data = False
        data = Configuration.isHoven(self.FamSize)
        if data!=False:
            self.appliancesList['Hoven']=Hoven(data)
            self.hasHoven = True
        
        #Iron
        data = False
        if Configuration.isIron(self.FamSize):
            self.appliancesList['Iron']=Iron()
        
        #Vacuumcleaner
        if Configuration.isVacuumclean(self.FamSize):
            self.appliancesList['Vacuumcleaner']=VacuumCleaner()
        
        #Boiler
        data = False
        data = Configuration.isEleBoiler(self.FamSize)
        if data!=False:
            self.appliancesList['Boiler']=WaterBoiler(data,self.personsList.keys())
            self.hasBoiler = True
        
        #Radio
        self.appliancesList['Radio']=Radio()

        #Microwavehome
        #if Configuration.isMicrowave(self.FamSize):
        #    self.appliancesList['Microwave']=Microwave()
        #    self.hasMicro = True

        #Cooking
        if Configuration.isElecooker(self.FamSize):
            self.appliancesList['Elecooker']=Elecooker()
            self.hasElecooker = True

        #Dryer
        if Configuration.isDryer(self.FamSize):
            self.appliancesList['Dryer']=Dryer()
            self.has_dryer = True

    def generate_house(self):
        self.generate_persons()
        self.generate_appliances()

class Simulator(object):
    """Simulates a number of ``Model`` models and collects some data."""
    def __init__(self):
        self.models = []
        self.data = []

    def add_model(self, init_val):
        """Create an instances of ``Model`` with *init_val*."""
        model = Home(init_val)
        self.models.append(model)
        self.data.append([])

    def step(self, time):	
        for i, model in enumerate(self.models):
            model.simulate_step(time)
@profile
def create():
    people={'hwife': 1,'inf': 1,'kid': 1,'no_work_f': 1,'no_work_m': 1,'old_f': 1,'old_m': 1,'stud': 1,'work_f': 1,
            'work_m':1,'full_m':1,'full_f':1,'partial_m':1,'partial_f':1}
    homes=[]
    start_time=time.time()
    for i in range(0,100):
        print (i,end='\r')
        homes.append(Home(people))
    print(time.time()-start_time)
    days=Configuration.getDays()
    for i in range(4):
        print(days[i],end='\r')
        for home in homes:
            home.simulate_step(days[i])

if __name__ == "__main__":
    create()
    '''
    people={'hwife': 1,'inf': 1,'kid': 2,'no_work_f': 0,'no_work_m': 0,'old_f': 0,'old_m': 0,'stud': 0,'work_f': 1,
            'work_m':1,'full_m':1,'full_f':0,'partial_m':0,'partial_f':0}

    home = Home(people) 
    home.generate_house()
    '''