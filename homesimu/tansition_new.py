from . semi_markov import NHSMM#as sm
import os
from memory_profiler import profile
path = os.path.dirname(__file__) + '/data/input/tran_matr'
class Transition(object):
    """docstring for Transition"""
    def __init__(self):
        super(Transition, self).__init__()
        self.infantWeek=NHSMM()
        self.infantSat=NHSMM()
        self.infantSun=NHSMM()
        self.infantWeek.load(os.path.join(path,'infant_week'))
        self.infantSat.load(os.path.join(path,'infant_sat'))
        self.infantSun.load(os.path.join(path,'infant_sun'))

        self.kidsWeek=NHSMM()
        self.kidsSat=NHSMM()
        self.kidsSun=NHSMM()
        self.kidsWeek.load(os.path.join(path,'kids_week'))
        self.kidsSat.load(os.path.join(path,'kids_sat'))
        self.kidsSun.load(os.path.join(path,'kids_sun'))

        self.studWeek=NHSMM()
        self.studSat=NHSMM()
        self.studSun=NHSMM()
        self.studWeek.load(os.path.join(path,'stud_week'))
        self.studSat.load(os.path.join(path,'stud_sat'))
        self.studSun.load(os.path.join(path,'stud_sun'))


        self.hwifeWeek=NHSMM()
        self.hwifeSat=NHSMM()
        self.hwifeSun=NHSMM()
        self.hwifeWeek.load(os.path.join(path,'hwife_week'))
        self.hwifeSat.load(os.path.join(path,'hwife_sat'))
        self.hwifeSun.load(os.path.join(path,'hwife_sun'))

        self.workmWeek=NHSMM()
        self.workmSat=NHSMM()
        self.workmSun=NHSMM()
        self.workmWeek.load(os.path.join(path,'work_m_week'))
        self.workmSat.load(os.path.join(path,'work_m_sat'))
        self.workmSun.load(os.path.join(path,'work_m_sun'))

        self.workfWeek=NHSMM()
        self.workfSat=NHSMM()
        self.workfSun=NHSMM()
        self.workfWeek.load(os.path.join(path,'work_f_week'))
        self.workfSat.load(os.path.join(path,'work_f_sat'))
        self.workfSun.load(os.path.join(path,'work_f_sun'))

        self.noworkmWeek=NHSMM()
        self.noworkmSat=NHSMM()
        self.noworkmSun=NHSMM()
        self.noworkmWeek.load(os.path.join(path,'no_work_m_week'))
        self.noworkmSat.load(os.path.join(path,'no_work_m_sat'))
        self.noworkmSun.load(os.path.join(path,'no_work_m_sun'))

        self.noworkfWeek=NHSMM()
        self.noworkfSat=NHSMM()
        self.noworkfSun=NHSMM()
        self.noworkfWeek.load(os.path.join(path,'no_work_f_week'))
        self.noworkfSat.load(os.path.join(path,'no_work_f_sat'))
        self.noworkfSun.load(os.path.join(path,'no_work_f_sun'))

        self.oldmWeek=NHSMM()
        self.oldmSat=NHSMM()
        self.oldmSun=NHSMM()
        self.oldmWeek.load(os.path.join(path,'old_m_week'))
        self.oldmSat.load(os.path.join(path,'old_m_sat'))
        self.oldmSun.load(os.path.join(path,'old_m_sun'))

        self.oldfWeek=NHSMM()
        self.oldfSat=NHSMM()
        self.oldfSun=NHSMM()
        self.oldfWeek.load(os.path.join(path,'old_f_week'))
        self.oldfSat.load(os.path.join(path,'old_f_sat'))
        self.oldfSun.load(os.path.join(path,'old_f_sun'))

        self.fullfWeek=NHSMM()
        self.fullfSat=NHSMM()
        self.fullfSun=NHSMM()
        self.fullfWeek.load(os.path.join(path,'fullwork_f_week'))
        self.fullfSat.load(os.path.join(path,'fullwork_f_sat'))
        self.fullfSun.load(os.path.join(path,'fullwork_f_sun'))

        self.fullmWeek=NHSMM()
        self.fullmSat=NHSMM()
        self.fullmSun=NHSMM()
        self.fullmWeek.load(os.path.join(path,'fullwork_m_week'))
        self.fullmSat.load(os.path.join(path,'fullwork_m_sat'))
        self.fullmSun.load(os.path.join(path,'fullwork_m_sun'))

        self.partialfWeek=NHSMM()
        self.partialfSat=NHSMM()
        self.partialfSun=NHSMM()
        self.partialfWeek.load(os.path.join(path,'partialwork_f_week'))
        self.partialfSat.load(os.path.join(path,'partialwork_f_sat'))
        self.partialfSun.load(os.path.join(path,'partialwork_f_sun'))

        self.partialmWeek=NHSMM()
        self.partialmSat=NHSMM()
        self.partialmSun=NHSMM()
        self.partialmWeek.load(os.path.join(path,'partialwork_m_week'))
        self.partialmSat.load(os.path.join(path,'partialwork_m_sat'))
        self.partialmSun.load(os.path.join(path,'partialwork_m_sun'))