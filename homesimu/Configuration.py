import pandas as pd
import numpy as np
import os
from memory_profiler import profile


start_date = '2015/01/01'
end_date = '2015/12/31 23:59'


appliances_res=10*60 #in seconds
behaviour_res=10 #in minutes
interval=int(60./appliances_res*behaviour_res)

data_path = os.path.dirname(__file__)
app_data=pd.read_csv(data_path + '/data/app_data.csv')


wash_cdf_week=pd.read_csv(data_path + '/data/wash_hwife_week_cdf.csv',index_col=[0],parse_dates=True,header=None)
wash_cdf_week.index=wash_cdf_week.index.time

wash_cdf_end=pd.read_csv(data_path + '/data/wash_hwife_end_cdf.csv',index_col=[0],parse_dates=True,header=None)
wash_cdf_end.index=wash_cdf_end.index.time

wash_cdf_work_week=pd.read_csv(data_path + '/data/wash_workf_week_cdf.csv',index_col=[0],parse_dates=True,header=None)
wash_cdf_work_week.index=wash_cdf_work_week.index.time

wash_cdf_work_end=pd.read_csv(data_path + '/data/wash_workf_end_cdf.csv',index_col=[0],parse_dates=True,header=None)
wash_cdf_work_end.index=wash_cdf_work_end.index.time

wash_cdf_man_week=pd.read_csv(data_path + '/data/wash_man_week_cdf.csv',index_col=[0],parse_dates=True,header=None)
wash_cdf_man_week.index=wash_cdf_man_week.index.time

wash_cdf_man_end=pd.read_csv(data_path + '/data/wash_man_end_cdf.csv',index_col=[0],parse_dates=True,header=None)
wash_cdf_man_end.index=wash_cdf_man_end.index.time


radiation=pd.read_csv(data_path + '/data/solar_rad.csv',index_col='Date_time')
radiation.index=pd.to_datetime(radiation.index)
radiation=radiation.tz_localize('Europe/Rome')
radiation=radiation.tz_convert('UTC')
rad_limit=60
radiation=radiation.resample('10T').mean().fillna(method='ffill')


dish_cdf_week=pd.read_csv(data_path + '/data/dish_hwife_week_cdf.csv',index_col=[0],parse_dates=True,header=None)
dish_cdf_week.index=dish_cdf_week.index.time

dish_cdf_end=pd.read_csv(data_path + '/data/dish_hwife_end_cdf.csv',index_col=[0],parse_dates=True,header=None)
dish_cdf_end.index=dish_cdf_end.index.time

dish_cdf_work_week=pd.read_csv(data_path + '/data/dish_workf_week_cdf.csv',index_col=[0],parse_dates=True,header=None)
dish_cdf_work_week.index=dish_cdf_work_week.index.time

dish_cdf_work_end=pd.read_csv(data_path + '/data/dish_workf_end_cdf.csv',index_col=[0],parse_dates=True,header=None)
dish_cdf_work_end.index=dish_cdf_work_end.index.time

dish_cdf_man_week=pd.read_csv(data_path + '/data/dish_man_week_cdf.csv',index_col=[0],parse_dates=True,header=None)
dish_cdf_man_week.index=dish_cdf_man_week.index.time

dish_cdf_man_end=pd.read_csv(data_path + '/data/dish_man_end_cdf.csv',index_col=[0],parse_dates=True,header=None)
dish_cdf_man_end.index=dish_cdf_man_end.index.time

#@profile
def getRadiation(time):
    return radiation.loc[time].values
    
    
def getDays():
    if end_date!=False:
        return pd.date_range(start_date, end_date,freq=str(behaviour_res)+'T',tz='UTC')
    return False
    


#devices

#Dishwasher
def isDishwash(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.Dishwasher[app_data.famSize==famSize].value_counts()/len(app_data.Dishwasher[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==1:
        prob_number=(app_data.DishWeek[(app_data.famSize==famSize) &( app_data.Dishwasher==1) ].value_counts()/len(app_data.DishWeek[(app_data.famSize==famSize) &( app_data.Dishwasher==1) ]))
        number=np.random.choice(prob_number.index,p=prob_number.values)
        prob_we=((app_data.DishWeekEnd[(app_data.famSize==famSize) &( app_data.DishWeek==number)]/app_data.DishWeek[(app_data.famSize==famSize)&(app_data.DishWeek==number)]).value_counts()/len(app_data.DishWeekEnd[(app_data.famSize==famSize) &( app_data.DishWeek==number)]/app_data.DishWeek[(app_data.famSize==famSize)&(app_data.DishWeek==number)]))
        number_we=number*np.random.choice(prob_we.index,p=prob_we.values)
        number=number-number_we
        return {'#w_cycle':number,'#wend_cycle':number_we}
    else:
        return False
def getDishTrace(efficency):
    return False
'''
def getdish_cdf(time,people):
    if people.has_key('Hwife_0') or people.has_key('NoWorkF_0') or people.has_key('OldF_0') and not (people.has_key('WorkF_0') or people.has_key('FullF_0') or people.has_key('WorkF_0') ):
        return dish_cdf.ix[time.time()].values[0]   
    else:
        return dish_cdf_work.ix[time.time()].values[0]

def getdish_cdf(people):
    if people.has_key('Hwife_0') or people.has_key('NoWorkF_0') or people.has_key('OldF_0') and not (people.has_key('WorkF_0') or people.has_key('FullF_0') or people.has_key('WorkF_0') ):
        return dish_cdf
    else:
        return dish_cdf_work
'''

def getdish_cdf(people,time):
    if 'Hwife_0' in people  or 'NoWorkF_0' in people or 'OldF_0' in people and not ( 'WorkF_0' in people or 'FullF_0' in people or 'PartialF_0' in people):
        if time.dayofweek<5:
            return dish_cdf_week
        else:
            return dish_cdf_end
    elif not ('WorkF_0' in people or 'FullF_0' in people or 'PartialF_0' in people or 'Hwife_0' in people or 'NoWorkF_0' in people or 'OldF_0' in people):
        if time.dayofweek<5:
            return dish_cdf_man_week
        else:
            return dish_cdf_man_end 
    else:
        if time.dayofweek<5:
            return dish_cdf_work_week
        else:
            return dish_cdf_work_end


#Washingmachine
def isWashing(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.WashingMachine[app_data.famSize==famSize].value_counts()/len(app_data.WashingMachine[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==1:
        prob_size=(app_data.WashingMachineSize[(app_data.famSize==famSize) & (app_data.WashingMachineSize!=0)].value_counts()/len(app_data.WashingMachineSize[(app_data.famSize==famSize) & (app_data.WashingMachineSize!=0)]))
        size=np.random.choice(prob_size.index,p=prob_size.values)
        prob_number=(app_data.WashWeek[(app_data.famSize==famSize) &( app_data.WashingMachineSize==size) ].value_counts()/len(app_data.WashWeek[(app_data.famSize==famSize) &( app_data.WashingMachineSize==size) ]))
        number=np.random.choice(prob_number.index,p=prob_number.values)
        prob_we=((app_data.WashWeekEnd[(app_data.famSize==famSize) &( app_data.WashWeek==number)]/app_data.WashWeek[(app_data.famSize==famSize)&(app_data.WashWeek==number)]).value_counts()/len(app_data.WashWeekEnd[(app_data.famSize==famSize) &( app_data.WashWeek==number)]/app_data.WashWeek[(app_data.famSize==famSize)&(app_data.WashWeek==number)]))
        number_we=number*np.random.choice(prob_we.index,p=prob_we.values)
        number=number-number_we
        return {'size':size,'#w_cycle':number,'#wend_cycle':number_we}
    else:
        return False
'''
def getwash_cdf(time,people):
    if people.has_key('Hwife_0') or people.has_key('NoWorkF_0') or people.has_key('OldF_0') and not (people.has_key('WorkF_0') or people.has_key('FullF_0') or people.has_key('WorkF_0') ):
        if time.dayofweek<5:
            return wash_cdf_week.ix[time.time()].values[0]
        else:
            return wash_cdf_end.ix[time.time()].values[0]
    else:
        if time.dayofweek<5:
            return wash_cdf_work_week.ix[time.time()].values[0]
        else:
            return wash_cdf_work_end.ix[time.time()].values[0]
''' 
def getwash_cdf(people,time):
    if 'Hwife_0' in people  or 'NoWorkF_0' in people or 'OldF_0' in people and not ( 'WorkF_0' in people or 'FullF_0' in people or 'PartialF_0' in people):
        if time.dayofweek<5:
            return wash_cdf_week
        else:
            return wash_cdf_end
    elif not ('WorkF_0' in people or 'FullF_0' in people or 'PartialF_0' in people or 'Hwife_0' in people or 'NoWorkF_0' in people or 'OldF_0' in people):
        if time.dayofweek<5:
            return wash_cdf_man_week
        else:
            return wash_cdf_man_end 
    else:
        if time.dayofweek<5:
            return wash_cdf_work_week
        else:
            return wash_cdf_work_end


def getWashTrace(efficency):
    return False

#Hoven
def isHoven(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.HovenType[app_data.famSize==famSize].value_counts()/len(app_data.HovenType[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==2:
        prob_size=(app_data.HovenSize[(app_data.famSize==famSize) & (app_data.HovenSize!=0)].value_counts()/len(app_data.HovenSize[(app_data.famSize==famSize) & (app_data.HovenSize!=0)]))
        size=np.random.choice(prob_size.index,p=prob_size.values)
        prob_number=(app_data.HovenUse[(app_data.famSize==famSize) &( app_data.HovenSize==size) ].value_counts()/len(app_data.HovenUse[(app_data.famSize==famSize) &( app_data.HovenSize==size) ]))
        number=np.random.choice(prob_number.index,p=prob_number.values)
        return {'size':size,'#w_cycle':number}
    else:
        return False

def getHovenTrace(efficency):
    return False

#Electric kitchen
def isElecooker(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.CookersType[app_data.famSize==famSize].value_counts()/len(app_data.CookersType[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==2:
        return True
    else:
        return False

def getElcoockerTrace(efficency):
    return False


#Firdge
def isFridge(famSize):
    if famSize>6:
        famSize=6
    prob_number=(app_data.FridgeNumber[app_data.famSize==famSize].value_counts()/len(app_data.FridgeNumber[app_data.famSize==famSize]))
    number=np.random.choice(prob_number.index,p=prob_number.values)
    if number!=0:
        prob_size=(app_data.FridgeSize[(app_data.famSize==famSize) & (app_data.FridgeSize!=0)].value_counts()/len(app_data.FridgeSize[(app_data.famSize==famSize) & (app_data.FridgeSize!=0)]))
        size=np.random.choice(prob_size.index,p=prob_size.values)
        if size==1:
            size=50
        if size==2:
            size=150
        if size==3:
            size=250
        if size==4:
            size==300
        return {'size':size}
    else:
        return False
def getFridgeTrace(efficency):  
    return False

#Electric Boiler
def isEleBoiler(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.ElectricBoiler[app_data.famSize==famSize].value_counts()/len(app_data.ElectricBoiler[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==1:
        prob_size=(app_data.ElectricBoilerSize[(app_data.famSize==famSize) & (app_data.ElectricBoilerSize!=0)].value_counts()/len(app_data.ElectricBoilerSize[(app_data.famSize==famSize) & (app_data.ElectricBoilerSize!=0)]))
        size=np.random.choice(prob_size.index,p=prob_size.values)
        if size==1:
            size=30
        elif size==2:
            size=60
        else:
            size=80
        return {'size':size}
    else:
        return False

#Personal Computer
def isDesktop(famSize):
    if famSize>6:
        famSize=6
    prob_number=(app_data.Desktop[app_data.famSize==famSize].value_counts()/len(app_data.Desktop[app_data.famSize==famSize]))
    number=np.random.choice(prob_number.index,p=prob_number.values)
    if number!=0:
        return number
    else:
        return False

def isLaptop(famSize):
    if famSize>6:
        famSize=6
    prob_number=(app_data.Laptop[app_data.famSize==famSize].value_counts()/len(app_data.Laptop[app_data.famSize==famSize]))
    number=np.random.choice(prob_number.index,p=prob_number.values)
    if number!=0:
        return number
    else:
        return False

def isTV(famSize):
    if famSize>6:
        famSize=6
    prob_number=(app_data.TVNumber[app_data.famSize==famSize].value_counts()/len(app_data.TVNumber[app_data.famSize==famSize]))
    number=np.random.choice(prob_number.index,p=prob_number.values)
    if number!=0:
        return number   
    else:
        return False

#Iron
def isIron(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.Iron[app_data.famSize==famSize].value_counts()/len(app_data.Iron[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==1:
        return True
    else:
        return False


#Vacuum
def isVacuumclean(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.VacuumCleaner[app_data.famSize==famSize].value_counts()/len(app_data.VacuumCleaner[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==1:
        return True
    else:
        return False

#Microwave
def isMicrowave(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.Microwave[app_data.famSize==famSize].value_counts()/len(app_data.Microwave[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==1:
        return True
    else:
        return False

def isDryer(famSize):
    if famSize>6:
        famSize=6
    prob_has=(app_data.Dryer[app_data.famSize==famSize].value_counts()/len(app_data.Dryer[app_data.famSize==famSize]))
    if np.random.choice(prob_has.index,p=prob_has.values)==1:
        return True
    else:
        return False
