from memory_profiler import profile
import pandas as pd
import mosaik_api
from homesimu import Home
import ray


META = {
    'models': {
        # We only expose the WECS agent as model to mosaik:
        'Home': {
            'public': True,
            'params': ['people','start_date'],
            'attrs': ['Presence','Absent','Aggregate','washday','people','has_wash','has_dish','has_dryer','hasBoiler','dishday','time_dish','time_wash'],
        },
    },
}

#@ray.remote
class HouseholdSim(mosaik_api.Simulator):
        def  __init__(self):
            super().__init__(META)
            self.simulator= Home.Simulator()
            self.eid_prefix = 'House_'
            self.timetosim=0
            self.entities = {}  # Maps EIDs to model indices in self.simulator
            self.cats_var = {'houseBehaviour':None, 'Consumption':None}  # mettere solo dict e liste


        def init(self,sid,start_date,eid_prefix=None):
            if eid_prefix is not None:
                self.eid_prefix = eid_prefix
            self.start_date = pd.to_datetime(start_date).tz_localize('UTC')
            return self.meta

        def create(self,num,model,people=None):
            next_eid = len(self.entities)
            entities = []
            people_keys = list(people.keys())
            for i in range(next_eid, next_eid + num):
                if len(people_keys)>1:
                    eid = '%s%s' % (self.eid_prefix, people_keys[i])

 #                   eid = '%s%s' % (self.eid_prefix, list(people.keys())[i])
                    self.simulator.add_model(people[people_keys[i]])
                    self.entities[eid] = i
                    entities.append({'eid': eid, 'type': model})
                else:
                    eid = '%s%s' % (self.eid_prefix, people_keys[0])
#                    eid = '%s%s' % (self.eid_prefix, ist(people.keys())[0])
                    self.simulator.add_model(people[people_keys[0]])                    
                    self.entities[eid] = i
                    entities.append({'eid': eid, 'type': model})

            return entities


        def step(self, time,input):
            self.timetosim=self.start_date + pd.Timedelta(time,unit='s')
            self.simulator.step(time=self.timetosim)

            return time + 10*60  # Step size is 10 minute

        def get_data(self, outputs):
            
            models = self.simulator.models
            data = {}
            # Variables list # TODO: @DS: non è un sistema molto efficiente
            
            for eid, attrs in outputs.items():
                model_idx = self.entities[eid]
                for cat in self.cats_var.keys():
                    self.cats_var[cat] = list(getattr(models[model_idx], cat).keys())
                data[eid] = {}
                for attr in attrs:
                    if attr not in self.meta['models']['Home']['attrs']:
                        raise ValueError('Unknown output attribute: %s' % attr)
                    exit=False
                    for cat, var in self.cats_var.items():
                        if attr in var:
                            data[eid][attr] = getattr(models[model_idx],cat)[attr]
                            exit=True
                            break
                    if exit==False:
                        data[eid][attr] = getattr(models[model_idx],attr)
                        
            return data

def main():
    return mosaik_api.start_simulation(HouseholdSim())


if __name__ == '__main__':
    main()