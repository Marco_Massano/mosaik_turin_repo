import mosaik_api
import geopandas as gpd
import collections
import pandas as pd
from gis_pv_sim import gis_pv_sim
import ray

META = {
    'models': {
        'PVSystem': {
            'public': True,
            'params': ['GisArea','FIDS','PVSspec'],
            'attrs': ['power_dc','n_mp','T_ext','zenith','k_b','k_d'],
        },
    },
}

#@ray.remote
class GISPVSIM(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)   
        self.simulator = gis_pv_sim.GISPVsimulator()
        self.eid_prefix = 'PV_'
        self.entities = {}  # Maps EIDs to model indices in self.simulator

    def init(self, sid, GisArea, start_date, eid_prefix=None):
        self.start_date = pd.to_datetime(start_date).tz_localize('UTC')
        self.simulator.GisArea=gpd.GeoDataFrame(GisArea)
        if eid_prefix is not None:
            self.eid_prefix = eid_prefix
        return self.meta

    def create(self, num, model,FIDS=[0],PVSspec=[0]):
        next_eid = len(self.entities)
        entities = []
        for i in range(next_eid, next_eid + num):
            eid = '%s%d' % (self.eid_prefix, FIDS[i])
            AreaPV={'FID':FIDS[i],'Area':self.simulator.GisArea.loc[FIDS[i],'Area_reale']}
            if len(PVSspec)>1:
                self.simulator.add_PV(AreaPV,PVSystemSpec=PVSspec[i])
                self.entities[eid] = i
                entities.append({'eid': eid, 'type': model})
            else:
                self.simulator.add_PV(AreaPV,PVSystemSpec=PVSspec[0])
                self.entities[eid] = i
                entities.append({'eid': eid, 'type': model})
        self.simulator.drop_not_used()
        return entities



    def step(self, time, inputs):
        # TODO finire comando step
        sim_time=self.start_date + pd.Timedelta(time,unit='s')
        data={}
        for eid , attrs in inputs.items():
            for attr, values in attrs.items():
                data[attr]=sum(values.values())
        # Perform simulation step
        data['time']=sim_time
        self.simulator.step(data)
        return time + 15 * 60  # Step size is 15 minute

    def get_data(self, outputs):
        models = self.simulator.models
       # print()
       # print(models)
        data = {}
        for eid, attrs in outputs.items():
            model_idx = self.entities[eid]
            data[eid] = {}
            #print(len(models),model_idx)
            for attr in attrs:
                if attr not in self.meta['models']['PVSystem']['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)
                data[eid][attr] = getattr(models[model_idx], attr)

        return data

#@ray.remote
def main():
    return mosaik_api.start_simulation(GISPVSIM())


if __name__ == '__main__':
    main()