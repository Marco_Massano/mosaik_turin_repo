#how to call from terminal: python my_start.py num_sec time(days)

import mosaik
import mosaik.util
import geopandas as gpd
import os
import pandas as pd
import numpy as np
import pickle
import itertools
import time
from pathlib import Path
import json

SIM_CONFIG = {
    'HouseholdSim': {
        'python': 'homesim_mosaik:HouseholdSim',
        #'cmd': 'python homesim_mosaik.py %(addr)s',
    },
    'GISPVSIM': {
        'python': 'mk_gis_pv_sim:GISPVSIM',
        #'cmd': 'python mk_gis_pv_sim.py %(addr)s',# 'host:5555',
    },
    "csv": {
      "python": "mk_csvsim:CSV"
    },

    'MeterSim': {
        'python': 'meter:MeterSim',
      #  'cmd': 'python meter.py %(addr)s',
    },
}

MK_CONFIG = {
    'addr': ('127.0.0.1', 5554),
    'start_timeout': 100,  # seconds default 10
    'stop_timeout': 100,  # seconds default 10
}


#path = os.path.dirname(__file__)
path = os.path.dirname('.')




######## SETTINGS

# Time interval
END = 3 # DEFINE THE NUMBER OF DAYS TO SIMULATE

END = END * 10 * 60 * 144
START_DATE = '2015-09-22 00:00:00' #YYYY/MM/dd 

# PVs Specifications
PVSystemsSpec = [{'Tc_noct':45, 'T_ex_noct':20, 'a_p':0.0038, 'ta':0.9, 'P_module':283, 'G_noct':800, 'G_stc':1000, 'ModuleArea':1.725, 'Tc_stc':25.0}]





######## IMPORT DATA

# Photovoltaic from DSM
gdf_PVs_tot = gpd.read_file('gis_pv_sim/area_turin/areasuit.shp', crs = '4326')
gdf_PVs_tot = gdf_PVs_tot.set_index(gdf_PVs_tot['FID'])
gdf_PVs_tot.crs = 'epsg:4326'
# Census Sections from ISTAT
gdf_section_tot = gpd.read_file('sez_cens/torino_sezcens.shp', crs = '32632')
gdf_section_tot = gdf_section_tot.sort_values(by = ['SEZ2011'])
gdf_section_tot = gdf_section_tot.to_crs(epsg = '4326')


# Houses from populationsim
houses_tot = pd.read_csv('pop_sim/synthetic_households.csv')
houses_tot = houses_tot.set_index(houses_tot['household_id'])
houses_tot = houses_tot.sort_values(by = ['SEZ2011'])

# Families from populationsim
fname = 'pop_sim/persons.pkl'
with open(fname, 'rb') as f:
    families_tot = pickle.load(f) 

# Meteo from WheatherUnderground    #########################à perchè non si fa un read_csv?
METEO_FILE = 'meteo_mosaik.csv'
METEO_PATH = os.path.join(path, METEO_FILE)





######## MANAGE DATA

# Restrict to a defined number of zones 
num_sec = 1 #DEFINE THE NUMBER OF SECTIONS TO SIMULATE

gdf_section = gdf_section_tot.iloc[:num_sec]

# Connect PVs with selected Section
gdf_PVs = gpd.sjoin(gdf_PVs_tot, gdf_section[['SEZ2011','geometry']],op='intersects')
gdf_PVs = gdf_PVs.sort_values(by =['SEZ2011'])

#gdf_PVs = gdf_PVs.loc[gdf_PVs['SEZ2011'].isin(gdf_section['SEZ2011'])]
houses = houses_tot.loc[houses_tot['SEZ2011'].isin(gdf_section['SEZ2011'])]

families  = {key: families_tot[key] for key in houses.index.astype(str)}

# Creation of PVs Dictionary
PVs_dict = {}
for i,r in gdf_PVs.iterrows():
    if ('Cens_' + str(r.SEZ2011)) not in PVs_dict.keys():
        PVs_dict['Cens_' + str(r.SEZ2011)] = []
    PVs_dict['Cens_' + str(r.SEZ2011)].append('PV_' + str(i))

# Creation of Houses Dictionary
houses_dict = {}
for i,r in houses.iterrows():
    if ('Cens_' + str(r.SEZ2011)) not in houses_dict.keys():
        houses_dict[('Cens_' + str(r.SEZ2011))] = []
    houses_dict[('Cens_' + str(r.SEZ2011))].append('House_' + str(i))




######## MOSAIK ENVIRONMENT

# World Definition
world = mosaik.World(SIM_CONFIG,MK_CONFIG)

# World Start
meteoSim = world.start('csv', sim_start = START_DATE, datafile = METEO_PATH)
homeSim = world.start('HouseholdSim', start_date = START_DATE)


pvSim = world.start('GISPVSIM', GisArea = gdf_PVs.to_dict(), start_date = START_DATE)
meterSim = world.start('MeterSim', start_date = START_DATE, collect_data = True)
meteo = meteoSim.meteo()  

# World Create
start_time = time.time()

homeModels = homeSim.Home.create(num = len(families), people = families)
time_houses = time.time() - start_time

pvModels = pvSim.PVSystem.create(num = len(gdf_PVs), FIDS = gdf_PVs.FID.values, PVSspec = PVSystemsSpec)
time_PVs = time.time() - start_time

meterModels = meterSim.MeterSystem.create(num = len(gdf_section), duration = int(END/10/60/144), num_sec = num_sec, ids = gdf_section.SEZ2011.values)
time_meters = time.time() - start_time
#

# World Connect
for meter in meterModels:
    if meter.eid in list(PVs_dict.keys()):
        for pv in PVs_dict[meter.eid]:
            world.connect(pvModels[world.sims['GISPVSIM-0']._inst.entities[pv]],meter, ('power_dc', 'Prod'))
    if meter.eid in list(houses_dict.keys()):
        for house in houses_dict[meter.eid]:
            world.connect(homeModels[world.sims['HouseholdSim-0']._inst.entities[house]],meter, ('Aggregate','Load'))

for model in pvModels:
    world.connect(meteo,model, 'T_ext', 'zenith', 'k_b', 'k_d')


time_connect = time.time() - start_time

    
# World Run 
world.run(until = END)
time_sim = time.time() - start_time
print(time_sim)
attributes = {}
attributes = {'num_sez' : int(len(gdf_section)),'num_PVs' : int(len(gdf_PVs)), 'num_houses' : int(len(houses)), 'time_houses' : round(time_houses,3), 'time_Pvs' : round(time_PVs,3), 'time_meters' : round(time_meters,3), 'time_connect' : round(time_connect,3), 'time_sim' : round(time_sim,3)}

Path("./OutputSimResults").mkdir(parents=True, exist_ok=True)
with open(f'./OutputSimResults/output_attributes_%d_days_%d_sez.json' % (int(END/10/60/144), num_sec), 'w') as outfile:
    json.dump(attributes, outfile)






